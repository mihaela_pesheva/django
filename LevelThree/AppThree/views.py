from django.shortcuts import render
from . import forms
# Create your views here.


def index(request):
    return render(request, 'AppThree/index.html')


def form_name_view(request):
    form = forms.FormName()

    if request.method == 'POST':
        form = forms.FormName(request.POST)

        if form.is_valid():
            print('Validation success!')
            print('name: ' + form.cleaned_data['name'])
            print('email: ' + form.cleaned_data['email'])
            print('text: ' + form.cleaned_data['text'])

    return render(request, 'AppThree/form_page.html', {'form': form})
